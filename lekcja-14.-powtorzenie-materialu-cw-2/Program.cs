﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lekcja_14._powtorzenie_materialu_cw_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int liczba = 4;

            for (int i = 1; i <= 5; i++)
            {
                Console.WriteLine("4 do potęgi {0} wynosi {1}", i, liczba);
                /* Wersja prosta */
                // liczba = liczba * 4;
                /* Wersja skrócona */
                liczba *= 4;
            }

            Console.ReadLine();
        }
    }
}
